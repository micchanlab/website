# Micchan Lab's website!

## About Micchan Lab
Micchan Lab is a laboratory where (mi|o)cchan explores various possibilities using a personal computer.

## URL
[https://micchan.com](https://micchan.com "Micchan Lab")

## E-mail
hello@micchan.com

Happy coding <3"
