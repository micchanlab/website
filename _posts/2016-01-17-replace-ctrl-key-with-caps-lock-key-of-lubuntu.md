---
layout: post
title: LubuntuでCtrlキーとCaps Lockキーを入れ替える
category: tips
tags: [lubuntu, keyboard]
---
Lubuntu（LXDE）で、CtrlキーとCaps Lockキーを入れ替えました。

環境：
[Lubuntu 15.10](http://lubuntu.net/)

エディタで*/etc/default/keyboard*を開きます。

`# vi /etc/default/keyboard`

8行目辺りにある**XKBOPTIONS=\"\"**に以下の内容を追加します。

{% highlight sh %}
XKBOPTIONS="ctrl:swapcaps"
{% endhighlight %}

※Caps lockキーを無効にする場合は**ctrl:nocaps**とします。

そして（ログアウトではなく）再起動をします。

以上で、CtrlキーとCaps Lockキーが入れ替わります。