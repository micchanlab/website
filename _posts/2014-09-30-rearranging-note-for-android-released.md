---
layout: post
title: することの順番を把握できる並び替えノート
category: android
tags: [note, rearranging]
---
というAndroidアプリを作りました。

アレをする前にコレしとかないと!!とか、先にコッチからしておこう!!など、することの順番を把握するのに役立つアプリです。
名前のとおり、並び替えが簡単にできるので、頭の中を整理しながら、すべきことを確認しましょう。

簡単なTODOリストとしてもお使いいただけます。

■主な使い方

上部バー内にテキストを入力し、ソフトキーボードの**完了**を押すと、リストの一番上に追加されます。

項目の右端を上下にスライドさせると、並び替えができます。

また、左右にスライドさせることで、削除ができます。削除・やり直しの確認が出るので再度スライドすると完全に削除されます。

パパっと片手での操作をイメージしています。

-----

[![並び替えノート](/assets/images/photos/2014-09-30/ic_launcher.png)](https://play.google.com/store/apps/details?id=com.micchan.rearrangingnote)

アプリ名： 並び替えノート

価格： 無料

-----

是非、使ってみて下さい :)

ダウンロードは[Google Play](https://play.google.com/store/apps/details?id=com.micchan.rearrangingnote)で！

*2015/01/06追記*

有料版（広告なし）を作りました。

-----

[![並び替えノート Plus](/assets/images/photos/2014-09-30/plus_ic_launcher.png)](https://play.google.com/store/apps/details?id=com.micchan.rearrangingnote.plus)

アプリ名： 並び替えノート Plus

価格： 110円

-----

ダウンロードは[Google Play](https://play.google.com/store/apps/details?id=com.micchan.rearrangingnote.plus)から!!