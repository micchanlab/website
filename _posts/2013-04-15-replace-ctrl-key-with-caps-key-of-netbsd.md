---
layout: post
title: NetBSDでCtrlキーとCaps Lockキーを入れ替える
category: tips
tags: [netbsd, keyboard]
---
日本語キーボードで、CtrlキーとCaps Lockキーを入れ替えました。

環境：
[NetBSD 6.0.1](https://www.netbsd.org/)

`# wsconsctl -w encoding=jp.swapctrlcaps`

エディタで*/etc/wscons.conf*を開き、上記と同様の設定を入れる。

`# vi /etc/wscons.conf`

以上で、CtrlキーとCaps Lockキーが入れ替わります。

USキーボードの場合は、**jp**の部分を**us**とします。



