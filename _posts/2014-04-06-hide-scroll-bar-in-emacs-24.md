---
layout: post
title: Emacsのスクロールバーを非表示にする
category: editor
tags: [emacs]
---
環境: 
[Linux Mint 16](https://www.linuxmint.com/)
と
[Gnu Emacs 24](https://www.gnu.org/software/emacs/)

*~/.emacs.d/init.el*に以下を追加
{% highlight cl %}
;; スクロールバーを非表示
(scroll-bar-mode 0)
{% endhighlight %}
