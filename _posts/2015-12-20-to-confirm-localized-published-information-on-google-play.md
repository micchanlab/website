---
layout: post
title: Google Playでローカライズした掲載情報を確認する
category: android
tags: [google play, localize]
---
Androidアプリ公開時に、日本語や英語などで掲載情報を作成した場合、その内容をウェブブラウザで確認する方法です。

※Playストアアプリで確認したい場合は、設定から端末の使用言語を変更する方法があります。

ウェブブラウザで表示する場合は、URLの末尾に「hl=ja」などのパラメーターを付けると表示されます。

例: https://play.google.com/store/apps?hl=ja

※一度パラメーターを付加すると、アプリのページを移動してもその言語のまま表示されます。

以下のリンクをクリックすることで、その言語でGoogle Playのアプリページが表示されます。

■ 日本語<br />
<a href="https://play.google.com/store/apps?hl=ja" target="_blank">https://play.google.com/store/apps?hl=ja</a>

■ 英語<br />
<a href="https://play.google.com/store/apps?hl=en" target="_blank">https://play.google.com/store/apps?hl=en</a>

■ スペイン語（スペイン）<br />
<a href="https://play.google.com/store/apps?hl=es" target="_blank">https://play.google.com/store/apps?hl=es</a>

■ スペイン語（ラテンアメリカ）<br />
<a href="https://play.google.com/store/apps?hl=es-419" target="_blank">https://play.google.com/store/apps?hl=es-419</a>

■ ドイツ語<br />
<a href="https://play.google.com/store/apps?hl=de" target="_blank">https://play.google.com/store/apps?hl=de</a>

■ フランス語<br />
<a href="https://play.google.com/store/apps?hl=fr" target="_blank">https://play.google.com/store/apps?hl=fr</a>

■ フランス語（カナダフランス語）<br />
<a href="https://play.google.com/store/apps?hl=fr-CA" target="_blank">https://play.google.com/store/apps?hl=fr-CA</a>

■ イタリア語<br />
<a href="https://play.google.com/store/apps?hl=it" target="_blank">https://play.google.com/store/apps?hl=it</a>

■ ポルトガル語（ブラジル）<br />
<a href="https://play.google.com/store/apps?hl=pt-br" target="_blank">https://play.google.com/store/apps?hl=pt-br</a>

■ 韓国語<br />
<a href="https://play.google.com/store/apps?hl=ko" target="_blank">https://play.google.com/store/apps?hl=ko</a>

■ 中国語（中国）<br />
<a href="https://play.google.com/store/apps?hl=zh-cn" target="_blank">https://play.google.com/store/apps?hl=zh-cn</a>

■ 中国語（香港）<br />
<a href="https://play.google.com/store/apps?hl=zh-hk" target="_blank">https://play.google.com/store/apps?hl=zh-hk</a>

■ 中国語（台湾）<br />
<a href="https://play.google.com/store/apps?hl=zh-tw" target="_blank">https://play.google.com/store/apps?hl=zh-tw</a>

■ ロシア語<br />
<a href="https://play.google.com/store/apps?hl=ru" target="_blank">https://play.google.com/store/apps?hl=ru</a>

上記リンク先で、検索等を使い自身のアプリ紹介ページにいくと、ローカライズされた情報を確認することができます。

以上です。