---
layout: post
title: UNIXコマンドのcatを使ってファイルを連結する
category: unix
tags: [command, cat, file]
---
UNIXコマンドを少しずつ学んでいます。

今日は、ファイルを表示したり、複数のファイルを連結するコマンド、*cat*について。

-----

## cat

読み方： かっと、きゃっと

機能： ファイルの内容を表示、複数のファイルを連結する

書式：

`cat (オプション) [ファイル名]`

オプション：

*-n* 行番号を付ける

*-b* 空行を含めず、行番号を付ける

*-s* 連続する空行を1行の空行にする

使用例：

（環境： [Linux Mint 14 Cinnamon](https://www.linuxmint.com/)）

ファイル(file01)の中身は...

===============

今日も

(・∀・)ｲｲ!!

天気です。

===============

です。このファイルに対して、行番号を表示する*-n*オプションをつけて*cat*を実行します。

`$ cat -n file01`

↓ 結果は...

{% highlight sh %}
    1 今日も
    2 (・∀・)ｲｲ!!
    3 天気です。
{% endhighlight %}

無事に行番号が表示されました。

次に、ファイルの連結です。

適当に*file02*と*file03*を用意し連結します。

`$ cat file02 file03 > file0203`

*file0203*の中身を表示させると...

`$ cat file0203`

 ↓

{% highlight sh %}
明日のことは、  # file02の内容
明後日考えよう。 # file03の内容
{% endhighlight %}

できました。

豆知識：

*cat*は、英語で連結することを意味する「[concatenate](http://translate.google.co.jp/#en/ja/concatenate)」の略。

-----

参考リンク:

1. [cat (UNIX) - Wikipedia](http://ja.wikipedia.org/wiki/Cat_%28UNIX%29 "cat (UNIX) - Wikipedia") <span class="url">(ja.wikipedia.org)</span>
2. [UNIXコマンド - cat](http://www.k-tanaka.net/unix/cat.php "UNIXコマンド - cat") <span class="url">(www.k-tanaka.net)</span>
3. [UNIXの部屋 コマンド検索:cat (\*BSD/Linux)](http://x68000.q-e-d.net/~68user/unix/pickup?cat "UNIXの部屋 コマンド検索:cat (\*BSD/Linux)") <span class="url">(x68000.q-e-d.net)</span>