---
layout: post
title: Androidでアクションバーの下にある影を消す
category: android
tags: [actionbar, null]
---
環境:
[Minimum SDK: API 15: Android 4.0.3(IceCreamSandwich)](https://developer.android.com)

アクションバーには、デフォルトで少しだけ影があります。

参考画像：

アクションバーの下に影がある状態↓

![アクションバーの下に影がある状態](/assets/images/photos/2014-10-11/shadow.png)

影がない状態↓

![アクションバーの下に影がない状態](/assets/images/photos/2014-10-11/no-shadow.png)

すこし分かりづらいですが...。

この影を取り除くには、*res/values*以下にある*styles.xml*に下記のコードを追加します。

{% highlight xml %}
<resources>
    <style name="AppTheme" parent="android:Theme.Holo.Light.DarkActionBar">
        <!-- 以下を追加 -->
        <item name="android:windowContentOverlay">@null</item>
    </style>
</resources>
{% endhighlight %}

-----

参考サイト：

1. [How can I have a drop shadow on my ActionBar (ActionBarSherlock)?](http://stackoverflow.com/questions/11448679/how-can-i-have-a-drop-shadow-on-my-actionbar-actionbarsherlock "How can I have a drop shadow on my ActionBar (ActionBarSherlock)?") <span class="url">(stackoverflow.com)</span>