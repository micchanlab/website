---
layout: post
title: BashにVimではなくviを起動させる
category: editor
tags: [vi, vim, bash]
---
Linux Mint 14のターミナルでviコマンドを実行するとVimが立ち上がりました。

Vimじゃなくviが使いたいので、設定を変更。

環境:
[Linux Mint 14 Cinnamon](https://www.linuxmint.com/)

まず一つ目の方法は、viコマンドの前に\を付ける。

`$ \vi example.sh`

これでviが起動できました。

次の方法は、Bashの設定ファイルを使うやり方。

*~/.bashrc*に以下を追加する。

{% highlight sh %}
# viを有効化する
set -o vi
{% endhighlight %}

.bashrcを変更した際は、sourceコマンドを忘れずに実行する。

`$ source ~/.bashrc`
