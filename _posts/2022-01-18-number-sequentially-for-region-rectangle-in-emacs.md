---
layout: post
title: Emacsで連番を振る
category: editor
tags: [emacs, tips]
---
Emacsでファイル内の任意の場所に連番を振る方法です。

環境: [Debian GNU/Linux 11](https://www.debian.org/ "Debian") + [GNU Emacs 27.1](https://www.gnu.org/software/emacs/index.html "GNU Emacs")

まず、*C-x SPACE*で矩形選択の状態にします。

次に連番を振りたい行数分、*C-n*で下に移動します。

その状態のまま下記のいずれかのキーを押下します。

1. *C-x r N*
2. *M-x rectangle-number-lines*

すると、1から始まる連番が右にスペース×1コとともに挿入されます。

複数の画像ファイルをImageMagickで縮小(convert -resize)した際に連番が振られるので、画像を挿入するHTMLソースの修正で上記の方法が役に立ちました。

以上です。

---

参考リンク:

1. [12.5 Rectangles](https://www.gnu.org/software/emacs/manual/html_node/emacs/Rectangles.html "12.5 Rectangles") <span class="url">(www.gnu.org)</span>
