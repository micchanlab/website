---
layout: post
title: 5分間隔で振動と通知音が鳴る5分間タイマー
category: android
tags: [timer]
---
というAndroidアプリを作りました。

5分毎に音と振動でお知らせしてくれるインターバルタイマーです。

お料理やトレーニングをする際にちょっと役立つかもしれません。

5分間で背景の色が変わります。あと、何分経ったかも5分単位で表示してくれます。

-----

[![5分間タイマー](/assets/images/photos/2013-10-13/ic_launcher.png)](https://play.google.com/store/apps/details?id=com.micchan.fiveminutetimer)

アプリ名： 5分間タイマー

価格： 無料

-----

是非、使ってみて下さい :)

ダウンロードは[Google Play](https://play.google.com/store/apps/details?id=com.micchan.fiveminutetimer)で