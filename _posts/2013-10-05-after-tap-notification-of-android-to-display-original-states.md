---
layout: post
title: Androidの通知をタップした時に元の状態を表示させる
category: android
tags: [acvitity, notification]
---
Notificationを利用してステータスバーに通知を表示した後、通知をタップした時にActivityを起動させず、元の状態を表示させる。

マニフェストファイル*AndroidManifest.xml*の**&lt;activity&gt;**内に以下を追加する。

`android:launchMode="singleTask"`

-----

参考サイト：

1. [Android PendingIntent take you to an already existing activity?](http://stackoverflow.com/questions/5465279/android-pendingintent-take-you-to-an-already-existing-activity "Android PendingIntent take you to an already existing activity?") <span class="url">(stackoverflow.com)</span>
2. [notification の利用 - （主に）プログラミングのメモ](http://yoshihikomuto.hatenablog.jp/entry/20111124/1322106813 "notification の利用 - （主に）プログラミングのメモ") <span class="url">(yoshihikomuto.hatenablog.jp)</span>
