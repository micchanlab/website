---
layout: post
title: Emacs上でファイル名を変更する
category: editor
tags: [emacs, dired, filename]
---
Emacs上で既存のファイル名を変更する。

環境:
[Lubuntu 15.04](https://lubuntu.net/)
と
[Gnu Emacs 24.4.1](https://www.gnu.org/software/emacs/)

1. ディレクトリエディタのDiredを起動
    * *C-x* *C-f* *RET*の順に入力する。
2. ファイル名にカーソルを移動
    * *C-n*や*C-p*などで変更するファイル名上にカーソルを移動する。
3. ファイル名上で「R」と入力
    * *Shift*を押したまま*r*を入力する。
4. ファイル名を変更
    * ミニバッファ内でファイル名を変更し*RET*を入力する。
5. Diredを終了
    * *q*を入力し終了する。

-----

参考サイト：

1. [編集中のファイル名変更は，Diredで - わさっき](http://d.hatena.ne.jp/takehikom/20111218/1324149099 "編集中のファイル名変更は，Diredで - わさっき") <span class="url">(d.hatena.ne.jp)</span>