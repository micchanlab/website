---
layout: post
title: すばやくメモが取れるPattoメモ
category: android
tags: [memo, quickly]
---
というAndroidアプリを作りました。

昨今のスマホ向けメモ帳アプリが、パソコンなどと同期することを前提としていたり、メモを取るまで何度か操作しないといけなかったりと、なんだか複雑なので、できる限りシンプルなものにしました。

■主な使い方

アプリを起動するとすぐにキーボードが出てくるので、上部のテキスト入力欄に文字を入力し、そのままエンターキーを押すことでリストに保存できます。

書いたメモを長押しすることで、共有や削除ができます。

長い文章にも対応してます。

-----

[![Pattoメモ](/assets/images/photos/2013-10-10/ic_launcher.png)](https://play.google.com/store/apps/details?id=com.micchan.pattomemo)

アプリ名： Pattoメモ

価格： 無料

-----

是非、使ってみて下さい :)

ダウンロードは[Google Play](https://play.google.com/store/apps/details?id=com.micchan.pattomemo)で！