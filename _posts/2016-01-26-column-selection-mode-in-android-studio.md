---
layout: post
title: Android Studioで矩形選択をする
category: android
tags: [android, development, tips]
---
Android Studio（IntelliJ）で矩形選択をする方法です。何通りかのやり方があります。

環境：
[Lubuntu 15.04](https://lubuntu.net/)
と
[Android Studio 1.5.1](https://developer.android.com/intl/ja/sdk/index.html)

■ 方法1<br />
マウスの真ん中ボタンを押しながら、ドラッグする

■ 方法2<br />
*Alt*を押しながら、マウスでドラッグする

<del>■ 方法3</del><br />
*Alt* + *Shift*を押しつつ、マウスの真ん中ボタンを押してドラッグする

上記の「方法3」は、一時的に通常の選択状態に戻すための方法でした。訂正します。(2016/02/02）

例えば、次にある「方法4」でカラム選択モード（矩形選択ができる）状態で*Alt* + *Shift*を押しながらドラッグすると、矩形選択ではなく、通常の選択になりました。

■ 方法4:<br />
上部にあるメニューの**Edit** → **Column Selection Mode**を選択してから、ドラッグする

※モード切り替えのショートカットは、*Alt* + *Shift* + *insert*です。

この環境では方法2と<del>3</del>は選択できませんでした。

-----

参考サイト：

1. [Selecting Text in the Editor - JetBrains](https://www.jetbrains.com/idea/help/selecting-text-in-the-editor.html#d1544049e320　"Selecting Text in the Editor - JetBrains") <span class="url">(www.jetbrains.com)</span>