---
layout: post
title: 線を描くとニャーオと音が鳴るねこ紙
category: android
tags: [cat drawing]
---
というAndroid向けアプリを作りました。

黒いキャンバスに線を描くと、ネコの鳴き声がします。実用性はあまりありませんが、どうしてもネコの絵が描きたくなるアプリです。

アイコン作りも頑張りました。

-----

[![ネコ紙](/assets/images/photos/2013-10-27/ic_launcher.png)](https://play.google.com/store/apps/details?id=com.micchan.catpaper)

アプリ名： ネコ紙

価格： 無料

-----

是非一度ダウンロードしてカワイイ鳴き声を確かめてみてください！

ダウンロードは[Google Play](https://play.google.com/store/apps/details?id=com.micchan.catpaper)で！