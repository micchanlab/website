---
layout: post
title: EmacsのDiredでファイルをコピーする
category: editor
tags: [emacs, tips]
---
EmacsのDiredで、単体のファイルをコピーする方法です。

環境: [Lubuntu 18.04](https://lubuntu.me) + [Gnu Emacs 25.2.2](https://www.gnu.org/software/emacs/)

シェルスクリプトを書いている時などに、編集前のファイルをバックアップしておきたい。

まず**C-x C-f**でDiredを起動後、該当のファイル上にカーソルを持っていき、以下のキーを押下する。

*C*

※Shiftキー + cキー

次に、ミニバッファがファイル名の入力を求めるので、適当な名前を入力し**RET**キーを押すとコピーしたファイルが作成される。

以上です。

-----

参考リンク:

1. [Operating on Files - GNU Emacs Manual](https://www.gnu.org/software/emacs/manual/html_node/emacs/Operating-on-Files.html "Operating on Files - GNU Emacs Manual") <span class="url">(www.gnu.org)</span>