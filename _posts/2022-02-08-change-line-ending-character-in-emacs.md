---
layout: post
title: Emacs上で改行コードを統一する
category: editor
tags: [emacs, tips]
---
LinuxとWindowsなど2つ以上のオペレーティングシステム環境で編集したファイルに複数の改行コードが混ざっていました。

検証環境: [Debian GNU/Linux 11 (bullseye)](https://www.debian.org/ "Debian") + [GNU Emacs 27.1](https://www.gnu.org/software/emacs/ "GNU Emacs")

Emacsは改行コードが混ざっているとそのファイル内の該当箇所に「^M」などと記号を表示してくれます。

これをどうするかによりますが、シンプルにすべての改行コードを同じに揃えるには、「^M」を置換する方法があります。

まず、*C-x h*で全選択状態にするか、ファイルの先頭にカーソルを移動します。

そして置換処理を開始するため、*M-x replace-string*をTABキー補完を活用しながら押下し*&lt;RET&gt;*。

ミニバッファが「Replace string:」となるので、*C-q C-m &lt;RET&gt;*の順に押下。

「Replace string ^M with:」と入力を求められるので、そのまま*&lt;RET&gt;*を押下します。

これでファイル内の「^M」記号がすべて置き換わります。この環境ではLFに統一されます。

参考サイト:

[Emacs: Newline Convention CR LF](http://xahlee.info/emacs/emacs/emacs_line_ending_char.html "Emacs: Newline Convention CR LF") <span class="url">(xahlee.info)</span>

上記サイトに詳しくありますが、内容を一部抜き出して書いてみます。

改行コード

| 名称             | 短縮形 | ASCII 10進法 | エスケープ表記 | キャレット表記 | Emacsでの入力方法 |
|-----------------|-------|--------------|-------------|--------------|-----------------|
| Line Feed       | LF    | 10           | \n          | ^J           | C-q C-j         |
| Carriage Return | CR    | 13           | \r          | ^M           | C-q C-m         |

Windows、Linux、Macでの違い

| Operating System      | 改行コード |
|-----------------------|----------|
| Unix, Linux, Mac OS X | LF       |
| Windows               | CR/LF    |
| Mac OS Classic        | CR       |


※C-x &lt;RET&gt;のあとにfで文字コードと改行コードを変更する方法はうまく作用しませんでした。。

また、諸事情によりファイル全体を修正できない場合は「^M」を非表示にするという手もあるみたいです。

実際はこっちのほうが現場で役立つかもしれません。\_(:3」∠)\_

[Hiding ^M in emacs - Stack Overflow](https://stackoverflow.com/questions/730751/hiding-m-in-emacs "[Hiding ^M in emacs - Stack Overflow]") <span class="url">(stackoverflow.com)</span>

{% highlight elisp %}
(defun remove-dos-eol ()
  "Do not show ^M in files containing mixed UNIX and DOS line endings."
  (interactive)
  (setq buffer-display-table (make-display-table))
  (aset buffer-display-table ?\^M []))
{% endhighlight %}

以上です(･∀･)