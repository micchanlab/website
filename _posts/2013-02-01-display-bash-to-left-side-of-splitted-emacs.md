---
layout: post
title: 2分割して起動したEmacsの左側にBashを表示する
category: editor
tags: [emacs, bash]
---
環境:
[Linux Mint 14](https://www.linuxmint.com/)
と
[Gnu Emacs 24](https://www.gnu.org/software/emacs/)

*~/.emacs.d/init.el*に以下を追加する。

{% highlight cl %}
;; 起動時にウィンドウを分割、左側にBashを表示
(defun split-window-and-run-shell()
  (setq w (selected-window))
  (setq w2 (split-window w nil t))
  (select-window w)
  (shell) ;; Bash
  (select-window w))
(add-hook 'after-init-hook (lambda()(split-window-and-run-shell)))
{% endhighlight %}

[Gnu Emacs Reference Mugs](http://shop.fsf.org/product/gnu-emacs-reference-mugs/ "Gnu Emacs Reference Mugs")
(Emacsのマグカップ♡)