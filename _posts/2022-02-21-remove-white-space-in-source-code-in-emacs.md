---
layout: post
title: Emacsで余分な空白を削除する
category: editor
tags: [emacs, tips]
---
ソースコードの行末や空行の先頭に残してしまった余分な空白を削除する方法です。

環境: [GNU Emacs 27.1](https://www.gnu.org/software/emacs/ "GNU Emacs")

下記のコマンドを実行する。

*M-x whitespace-cleanup*

または、選択範囲内のみ削除するには、

*M-x whitespace-cleanup-region*

とする。

余分な空白（ホワイトスペース）を表示するには*M-x whitespace-mode*とします。

以上です。