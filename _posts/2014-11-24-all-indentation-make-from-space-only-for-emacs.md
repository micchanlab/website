---
layout: post
title: Emacsのタブを無効化しスペースを挿入する
category: editor
tags: [emacs]
---
*~/.emacs.d/init.el*に以下を追加する。
{% highlight cl %}
;; タブの無効化
(setq-default indent-tabs-mode nil)
{% endhighlight %}

この設定で、インデントをした時に、タブではなくスペースが挿入されるようになります。

また、**M-x untabify**で、ファイル内のタブをスペースに変換することもできます。

全てのタブをスペースに変換したいときは、**C-x h**で全選択をしたあとに行うとスムーズです。

-----

参考リンク：

1. [Just Spaces - GNU Emacs Manual](https://www.gnu.org/software/emacs/manual/html_node/emacs/Just-Spaces.html "Just Spaces - GNU Emacs Manual") <span class="url">(www.gnu.org)</span>
2. [Indent Tabs Mode - Programming in Emacs Lisp](https://www.gnu.org/software/emacs/manual/html_node/eintr/Indent-Tabs-Mode.html "Indent Tabs Mode - Programming in Emacs Lisp") <span class="url">(www.gnu.org)</span>