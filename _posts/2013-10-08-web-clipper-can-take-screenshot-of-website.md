---
layout: post
title: ウェブサイトのスクリーンショットを撮るウェブクリッパー
category: android
tags: [web, screenshot]
---
というAndroid向けアプリを作りました。

気に入った文章が書いてあったり、ユニークなデザインのウェブページなどがあったら、そのまま画像として保存することができます。

画像はギャラリーアプリに保存されるので、いつでも見返したり、メールに添付して共有することができます。

■主な使い方

ブラウザなどにある**共有する**のアプリ一覧から、ウェブクリッパーを選択します。

するとウェブページが表示されるので、上部にあるカメラボタンを押しスクリーンショットを撮ります。

バーに*http://*から始まるURLを貼り付けたり、任意のキーワードを入れることでウェブページを開くこともできます。

ちょっとしたウェブブラウザとしてもお使いいただけます。

-----

[![ウェブクリッパー](/assets/images/photos/2013-10-08/ic_launcher.png)](https://play.google.com/store/apps/details?id=com.micchan.webclipper)

アプリ名：ウェブクリッパー

価格：無料

-----

是非、使ってみて下さい :)

ダウンロードは[Google Play](https://play.google.com/store/apps/details?id=com.micchan.webclipper)で！

*2015/01/16追記*

有料版（広告なし）を作りました!!

-----

[![ウェブクリッパー Plus](/assets/images/photos/2013-10-08/plus_ic_launcher.png)](https://play.google.com/store/apps/details?id=com.micchan.webclipper.plus)

アプリ名：ウェブクリッパー Plus

価格：110円

-----

ダウンロードは[Google Play](https://play.google.com/store/apps/details?id=com.micchan.webclipper.plus)から！