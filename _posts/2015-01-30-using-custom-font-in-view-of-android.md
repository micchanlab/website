---
layout: post
title: AndroidのViewで独自フォントを使用する
category: android
tags: [font, custom]
---
TextViewに表示する文字に、独自のフォントを適用させてみます。

環境：
[Minimum SDK: API 15: Android 4.0.3(IceCreamSandwich)](https://developer.android.com/)

まず、使用するフォントを用意する。今回は、[Google Fonts](https://www.google.com/fonts/ "Google Fonts")からダウンロードしてきた「ReenieBeanie.ttf」を使用します。

次に、Androidプロジェクトのフォルダ内に、上記のフォントファイルを設置します。

*プロジェクト名/app/src/main/*以下に*assets*フォルダを新たに作成し、その中にフォントファイルを設置します。

![フォントファイルの設置](/assets/images/photos/2015-01-30/font-file-in-assets-folder.png)

次に、レイアウトファイル内に、独自フォントを適用したいTextViewを追加する。

ここでは、*res/layout/activity_main.xml*に追加します。

{% highlight xml %}
<RelativeLayout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    tools:context=".MainActivity">

    <!-- 以下を追加する -->
    <TextView
        android:id="@+id/custom_font_text"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:text="@string/hello_world"
        android:textSize="48sp"
        />

</RelativeLayout>
{% endhighlight %}

そして、フォントを適用させるプログラムを書きます。

今回は、*MainActivity.java*です。

{% highlight java %}
public class MainActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        /* 以下を追加する */
        TextView txt = (TextView) findViewById(R.id.custom_font_text);
        Typeface font = Typeface.createFromAsset(getAssets(), "ReenieBeanie.ttf");
        txt.setTypeface(font);
    }
}
{% endhighlight %}

これを実行すると、以下のようになります。

![実行結果](/assets/images/photos/2015-01-30/result-custom-font.png)

これで以上です。

-----

参考サイト：

1. [Quick Tip: Customize Android Fonts - Tuts+ Code Tutorial](http://code.tutsplus.com/tutorials/customize-android-fonts--mobile-1601 "Quick Tip: Customize Android Fonts - Tuts+ Code Tutorial") <span class="url">(code.tutsplus.com)</span>