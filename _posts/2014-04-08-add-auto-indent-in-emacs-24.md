---
layout: post
title: Emacsにオートインデント機能を付加する
category: editor
tags: [emacs]
---
環境:
[Linux Mint 16](https://www.linuxmint.com/)
と
[GNU Emacs 24.3](https://www.gnu.org/software/emacs/)

Emacsのバージョンが24.3以降であれば、改行する際に**RET**キーではなく**C-j**を使用することで、自動的にインデントされます。

**RET**キーに割り当てる場合は、*~/.emacs.d/init.el*に以下を追加する。

{% highlight cl %}
;; RETキーでオートインデント
(define-key global-map (kbd "RET") 'newline-and-indent)
{% endhighlight %}

-----

参考リンク:

1. [EmacsWiki: Auto Indentation](http://www.emacswiki.org/emacs/AutoIndentation "EmacsWiki: Auto Indentation")&nbsp;<span class="url">(www.emacswiki.org)</span>