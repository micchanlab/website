---
layout: post
title: EmacsのDiredで表示内容を更新する
category: editor
tags: [emacs, tips]
---
EmacsのDiredを使っていて、コマンドラインから新たにファイルを作成した時に、そのままでは作成したファイルが表示されず、表示内容の更新が必要でした。

環境: [Uubuntu Server 17.04](https://www.ubuntu.com/server) + [lubuntu-desktop](https://lubuntu.net/)、[Gnu Emacs 25.1.1](https://www.gnu.org/software/emacs/)

新規にファイルを作成したあと、Diredを使用する時に以下のコマンドを実行することで表示内容がアップデートされる。

*g*

これでDiredのバッファにある全てのコンテンツが更新され、作成したファイルが表示されます。

-----

参考リンク:

1. [Dired Updating - GNU Emacs Manual](https://www.gnu.org/software/emacs/manual/html_node/emacs/Dired-Updating.html "Dired Updating - GNU Emacs Manual") <span class="url">(www.gnu.org)</span>