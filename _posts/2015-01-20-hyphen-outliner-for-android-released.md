---
layout: post
title: 発想をまとめておくためのハイフンアウトライナー
category: android
tags: [idea, outliner, processor]
---
というAndroid向けアプリを作りました。

プレーンテキスト形式のアウトライン・プロセッサです。

エンターキーを押すと、自動でハイフン文字とスペースが挿入されます。さくさくと書き込むことができるので、アイデアを自由に発想するときに役立ちます。

また、メールで送信したり、SDカードに保存したり（.txtファイル）できます。

【こんな時に使うといいかも】

■ 一人でじっくり考え事をしているとき

⇒ 次々に思いつくことをどんどん書き込んで整理しよう!!

■ 長い文章を書きたいが、うまくまとまらない時xP

⇒ 小分けにして考えるといいかも:)

■ 考え抜いたアイデアを冷静に分析したい時

⇒ 他の人にもメールで送って見てもらおう(^^)

【こんな方にもおすすめ】

□ メモやTODOなどは主にテキストファイルで管理している

□ 紙を節約したい方、デスクの上をキレイにしたい方

□ シンブルでミニマムなものが好きな方

-----

[![ハイフンアウトライナー](/assets/images/photos/2015-01-20/ic_launcher.png)](https://play.google.com/store/apps/details?id=com.micchan.hyphenoutliner)

アプリ名： ハイフンアウトライナー

価格： 230円

-----

ぜひ使ってみてください!!

ダウンロードは[Google Play](https://play.google.com/store/apps/details?id=com.micchan.hyphenoutliner)から :)

*2015年2月18日追記*

無料版を作りました。

-----

[![ハイフンアウトライナー Lite](/assets/images/photos/2015-01-20/ic_launcher_lite.png)](https://play.google.com/store/apps/details?id=com.micchan.hyphenoutliner.lite)

アプリ名： ハイフンアウトライナー Lite

価格： 無料

-----

[Google Play](https://play.google.com/store/apps/details?id=com.micchan.hyphenoutliner.lite)からダウンロードできます!!

参考：

1. [アウトライン・プロセッサ - Wikipedia](http://ja.wikipedia.org/wiki/%E3%82%A2%E3%82%A6%E3%83%88%E3%83%A9%E3%82%A4%E3%83%B3%E3%83%97%E3%83%AD%E3%82%BB%E3%83%83%E3%82%B5)&nbsp;<span class="url">(jp.wikipedia.org)</span>