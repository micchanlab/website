---
layout: post
title: AndroidのListViewでスクロールバーを右端に配置する
category: android
tags: [listview, scrollbar]
---
ListViewに余白（paddingなど）を付けた場合でも、スクロールバーの位置を画面の右端に配置したい。

また、スクロールした時に、上下の余白部分が気になるので、見栄え良く上下の端までスクロールしてほしい。

これらを解決するには、ListViewの属性に以下の2つを追加します。

1. clipToPadding=\"false\"
   * 描画範囲をpadding領域内に制限しない。デフォルトはtrue。
2. scrollbarStyle=\"outsideOverlay\"
   * スクロールバー用の領域を確保せず(overlay)、バーをpadding領域の外側に揃える(outside)。

例：

{% highlight xml %}
<ListView
    android:id="@+id/my_list"
    android:layout_width="match_parent"
    android:layout_height="wrap_content"
    android:padding="16dp"
    android:clipToPadding="false"
    android:scrollbarStyle="outsideOverlay"
    />
{% endhighlight %}

下にある参考サイトにて、付加する前と後の違いをgif画像で確認できます。

-----

参考サイト：

1. [Today's #AndroidDev and #AndroidDesign #Protip from +Roman Nurik is about...](https://plus.google.com/+AndroidDevelopers/posts/LpAA7q4jw9M "Today's #AndroidDev and #AndroidDesign #Protip from +Roman Nurik is about...") <span class="url">(plus.google.com)</span>