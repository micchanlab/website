---
layout: post
title: 顔をアップで映し出すフェイスチェッカー
category: android
tags: [application, camera]
---
というAndroidアプリを作りました。

お化粧直しや、お鼻毛が出ていないか、目ヤニがついてないかをチェックできます。

普通のインカメラより顔が拡大され、鼻の辺りが真ん中に映るようにしてます。

-----

[![フェイスチェッカー](/assets/images/photos/2013-10-01/ic_launcher.png)](https://play.google.com/store/apps/details?id=com.micchan.facechecker)

アプリ名： フェイスチェッカー

価格： 無料

-----

是非ダウンロードして使ってみて下さい :)

ダウンロードは[Google Play](https://play.google.com/store/apps/details?id=com.micchan.facechecker)で。