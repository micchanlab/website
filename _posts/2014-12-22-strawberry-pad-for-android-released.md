---
layout: post
title: ストロベリーパッドは書きたくなるノート
category: android
tags: [note, notepad]
---
ストロベリーパッドというAndroidアプリを作りました。

■どんなアプリ??

多目的な用途に対応できるノートアプリです。

考えるための道具として、必要最小限の機能だけを実装しています。

シンプルなデザインと操作性は、つい何かを書き込みたくなるはず!?

ツールバーは鮮やかなストロベリー色です。

■こんな使い方

1. プロジェクト毎のメモ
2. TODOリスト
3. 日記・日誌
4. メール本文のテンプレート集

■主な使い方と特徴

*＋*ボタンで、新しいノートを追加します。

ノートは自動で保存されます。

また、メールで送信したり、SDカードに保存したり（.txtファイル）できます。

-----

[![ストロベリーパッド](/assets/images/photos/2014-12-22/ic_launcher.png)](https://play.google.com/store/apps/details?id=com.micchan.strawberrypad)

アプリ名： ストロベリーパッド

価格： 230円

-----

是非、使ってみて下さい :)

ダウンロードは[Google Play](https://play.google.com/store/apps/details?id=com.micchan.strawberrypad)で！

*2015年1月2日追記*

無料版を作りました。

-----

[![ストロベリーパッド Lite](/assets/images/photos/2014-12-22/lite_ic_launcher.png)](https://play.google.com/store/apps/details?id=com.micchan.strawberrypad.lite)

アプリ名： ストロベリーパッド Lite

価格： 無料

-----

ダウンロードは[Google Play](https://play.google.com/store/apps/details?id=com.micchan.strawberrypad.lite)から！