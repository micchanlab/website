---
layout: post
title: 初めてのLinux向けアプリ開発
category: programming
tags: [linux, c, gtk+, gnome]
---
デスクトップに[Xpad](https://launchpad.net/xpad "Xpad")を常に表示させて、メモを残しています。

シンプルで使いやすいのですが、こんな機能がついてたらなーと思うこともしばしば。

そこで、C言語の勉強も兼ねて、メモアプリを作ってみることにしました。

今回は、Linux向けのアプリ開発がどんなものなのか調べつつ、超簡易メモパッドを作ってみます。

環境: [Linux Mint 14 Cinnamon](https://www.linuxmint.com/)

流れ:

1. 開発環境を整える
2. チュートリアルを見る
3. コードを書く
4. 出来上がりを確認する
5. 配布用にパッケージングする

まず開発環境を整えるところから。

[Install GNOME development environment on Ubuntu](https://live.gnome.org/DeveloperTools/Installation/Ubuntu "Install GNOME development environment on Ubuntu")（英語）を参考に、ターミナルを起動して以下を実行する。

`$ sudo apt-get install anjuta glade devhelp`

1. Anjuta（アニュータ）: 統合開発環境（IDE）
    * 公式サイト: [http://projects.gnome.org/anjuta/](http://projects.gnome.org/anjuta/ "Anjuta")（英語）
    * ウィキペディア: [http://ja.wikipedia.org/wiki/Anjuta](http://ja.wikipedia.org/wiki/Anjuta)
2. Glade: インターフェイスデザイナー
    * 公式サイト: [http://glade.gnome.org/](http://glade.gnome.org/ "Glade")（英語）
    * ウィキペディア: [http://ja.wikipedia.org/wiki/Glade_Interface_Designer](http://ja.wikipedia.org/wiki/Glade_Interface_Designer)
3. Devhelp: APIブラウザー
    * 公式サイト: [https://live.gnome.org/devhelp](https://live.gnome.org/devhelp)（英語）

次に、C言語を使うため、GNOMEライブラリをインストールする。

`$ sudo apt-get install libgtk-3-dev libgstreamer0.10-dev libclutter-1.0-dev libwebkitgtk-3.0-dev libgda-5.0-dev`

ドキュメントも。

`$ sudo apt-get install libgtk-3-doc gstreamer0.10-doc libclutter-1.0-doc libgda-5.0-doc`

チュートリアルは[GNOME 開発センター](http://developer.gnome.org/ "GNOME 開発センター")の10分チュートリアルというのをしてみました。チュートリアル自体は英語のみのようです。画像とかもあります。

実際に作ってみる。

まずデスクトップの**メニュー**から**プログラミング** > **Anjuta**でAnjutaを起動する。

![Anjutaを起動する](/assets/images/photos/2013-02-03/2013-02-03-launch-anjuta.png)

**ファイル** > **新規** > **4. プロジェクト**を選択。

![ファイル > 新規 > 4. プロジェクトを選択](/assets/images/photos/2013-02-03/2013-02-03-anjuta-new-project.png)

**C**タブから**GTK+（アプリケーション）**を選択し、**続行**をクリック。

![GTK+アプリケーション](/assets/images/photos/2013-02-03/2013-02-03-anjuta-c-gtk-application.png)

「基本的な情報」が表示されるので、入力して**続行**をクリック。

1. プロジェクト名: 自分のアプリ名 (e.g. memo)
2. 作者: 名前 (e.g. micchan)
3. E-メールアドレス: メールアドレスを入力
4. バージョン: 0.1とか

「プロジェクトのオプション」で、ライセンスなど色々決める。今回はディレクトリの場所だけ任意の場所に変更。決めたら**続行**します。

「サマリ」が表示され、情報の確認をしたら**適用(A)**をクリック。

プロジェクトのページに移動するので、左の真ん中あたりにある**ファイル**をクリックし、*src/main.c*を開く。

![main.c](/assets/images/photos/2013-02-03/2013-02-03-anjuta-main-c.png)

[TextView widget - GNOME DEV CENTER](http://developer.gnome.org/gnome-devel-demos/unstable/textview.c.html.en "TextView widget")を参考にしながら*src/main.c*にコードを書き加える。

{% highlight c %}
#include <config.h>
#include <gtk/gtk.h>
#include "memo.h"

#include <glib/gi18n.h>

/*
 * 以下の部分を追加する。
 */
static void
activate (GtkApplication *app,
          gpointer        user_data)
{
  /* 変数の定義 */
  GtkWidget *window;
  GtkWidget *text_view;
  GtkWidget *scrolled_window;

  GtkTextBuffer *buffer;

  /* タイトル付きのウィンドウをつくって、サイズを決める */
  window = gtk_application_window_new (app);
  gtk_window_set_title (GTK_WINDOW (window), "Memo");
  gtk_window_set_default_size (GTK_WINDOW (window), 320, 500);

  /* テキストバッファを作成する */
  buffer = gtk_text_buffer_new (NULL);

  /* テキストビューのウィジェットを作成する 
   * ラインラッピングを適用する
   */
  text_view = gtk_text_view_new_with_buffer (buffer);
  gtk_text_view_set_wrap_mode (GTK_TEXT_VIEW (text_view), GTK_WRAP_WORD); 

  /* スクロール可能なウィンドウを作る */
  scrolled_window = gtk_scrolled_window_new (NULL, NULL);
  gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (scrolled_window), 
                                  GTK_POLICY_AUTOMATIC, 
                                  GTK_POLICY_AUTOMATIC); 
  /* 組み立てる */
  gtk_container_add (GTK_CONTAINER (scrolled_window), 
                                         text_view);
  gtk_container_set_border_width (GTK_CONTAINER (scrolled_window), 0);
 
  gtk_container_add (GTK_CONTAINER (window), scrolled_window);

  gtk_widget_show_all (window);
}
/* 追加ここまで */

int
main (int argc, char *argv[])
{
	Memo *app;
	int status;

#ifdef ENABLE_NLS
	bindtextdomain (GETTEXT_PACKAGE, PACKAGE_LOCALE_DIR);
	bind_textdomain_codeset (GETTEXT_PACKAGE, "UTF-8");
	textdomain (GETTEXT_PACKAGE);
#endif

	app = memo_new ();
	/* 下の一行も追加する */
	g_signal_connect (app, "activate", G_CALLBACK (activate), NULL);
	status = g_application_run (G_APPLICATION (app), argc, argv);
	g_object_unref (app);

	return status;
}
{% endhighlight %}

書き終えたら、メニューから**ビルド** > **プロジェクトのビルド**を選択する。

最初だけ「プロジェクトの構成」のポップアップが出てくるので、今回はそのまま**実行**をクリック。

下部のメッセージにガーっと処理が表示されます。

「完了しました（成功です）」とでればOK。失敗が表示されたら、メッセージの内容をヒントに間違っているところを修正します。

次に、メニューから**実行** > **実行**をクリック。

こんなのが出ます。（もうひとつ空のウィンドウがでてきますが気にしません...）

![初めてのメモアプリ](/assets/images/photos/2013-02-03/2013-02-03-memo-app-for-linux.png)

簡易メモパッドができあがりました。

最後に、アプリを配布するためのパッケージング方法。

まず、メニューから**ビルド** > **構成の選択** > **デフォルト**を選択し、もう一度**プロジェクトのビルド**をします。

次に、同じ**ビルド**の一番下にある**Tarballの生成**をクリック。

すると、プロジェクトのディレクトリにTarball（e.g. memo-0.1.tar.gz）ができています。

![memo-0.1.tar.gz](/assets/images/photos/2013-02-03/2013-02-03-memo-tar-gz.png)

これで以上です。
